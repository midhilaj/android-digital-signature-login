package in.exalture.signature;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kyanogen.signatureview.SignatureView;

import java.util.List;

import in.exalture.signature.model.User;

public class MainActivity extends AppCompatActivity {
    SignatureView signatureView;
    Button login;
    FloatingActionButton fab;
    EditText username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       init();
    }
    Runnable r1;
    Bitmap s1;
    private void init(){
        signatureView = (SignatureView) findViewById(R.id.signature_view);
        username=(EditText)findViewById(R.id.username);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signatureView.clearCanvas();
            }
        });

        login=(Button)findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(username.getText().toString().length()<=3){
                    username.setError("Please enter a valied user name");
                    username.requestFocus();
                }else {
                    final int[] sct = {0};
                    r1=     new Runnable() {
                        @Override
                        public void run() {
                            s1=signatureView.getSignatureBitmap();
                            for(int i=0;i<s1.getWidth();i++){
                                for(int j=0;j<s1.getHeight();j++){
                                    int x=i;
                                    int y=j;


                                    int pixel = s1.getPixel(x,y);
                                    if(pixel!=-1){
                                        sct[0]++;
                                        Log.i("pixel",x+","+y+" = "+pixel);//Toast.makeText(getApplicationContext(),"yes "+pixel,Toast.LENGTH_SHORT).show();
                                    }
                                    int redValue = Color.red(pixel);
                                    int blueValue = Color.blue(pixel);
                                    int greenValue = Color.green(pixel);

                                }
                            }

                        // List<User> ul=User.listAll(User.class);
                            List<User> ul  =User.find(User.class, "username = ? ", username.getText().toString().trim());
                            if( sct[0]!=0){
                                if(ul!=null?ul.size()>0?true:false:false){
                                    Log.i("login_value",sct[0]+","+ul.get(0).getSig());
                                    if(ul.get(0).getSig()==sct[0]||sct[0]==ul.get(0).getSig()){

                                        ul.get(0).setLogedin(true);
                                        ul.get(0).save();;
                                        finish();startActivity(new Intent(getApplicationContext(),HomePage.class));
                                    }else
                                    if(ul.get(0).getSig()-sct[0]>-1)
                                    {
                                        if(ul.get(0).getSig()-sct[0]<50&&ul.get(0).getSig2()-sct[0]<50){


                                            ul.get(0).setLogedin(true);
                                            ul.get(0).save();;
                                            finish();startActivity(new Intent(getApplicationContext(),HomePage.class));


                                        }else {
                                            {
                                                Toast.makeText(getApplicationContext(),"login failed",Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }else {
                                        if(sct[0]-ul.get(0).getSig()<50&&sct[0]-ul.get(0).getSig2()<50){


                                            ul.get(0).setLogedin(true);
                                            ul.get(0).save();;
                                            finish();
                                            startActivity(new Intent(getApplicationContext(),HomePage.class));


                                        }else {
                                            {
                                                Toast.makeText(getApplicationContext(),"login failed",Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                                }else {
                                    Toast.makeText(getApplicationContext(),"user not found",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(),"login failed",Toast.LENGTH_SHORT).show();
                            }

                            signatureView.clearCanvas();
                        }
                    };
                    r1.run();
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        r1=null;
        s1=null;

    }
}
