package in.exalture.signature;

/**
 * Created by midhilaj on 1/25/17.
 */

import android.content.Context;

import com.orm.SugarApp;

public class App extends SugarApp {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }


    @Override
    public void onCreate() {
        super.onCreate() ;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    } //you can leave this empty or override methods if you like so the thing is that you need to extend from MultiDexApplication
}