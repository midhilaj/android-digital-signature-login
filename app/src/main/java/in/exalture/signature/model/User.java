package in.exalture.signature.model;

import android.support.annotation.Keep;

import com.orm.SugarRecord;
@Keep
public class User extends SugarRecord<User> {
    String username;

    public boolean isLogedin() {
        return logedin;
    }

    public void setLogedin(boolean logedin) {
        this.logedin = logedin;
    }

    boolean logedin=false;
    public User(){

    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getSig() {
        return sig;
    }

    public void setSig(int sig) {
        this.sig = sig;
    }

    int sig;

    public int getSig2() {
        return sig2;
    }

    public void setSig2(int sig2) {
        this.sig2 = sig2;
    }

    int sig2;

}
