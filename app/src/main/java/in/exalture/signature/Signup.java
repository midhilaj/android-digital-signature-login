package in.exalture.signature;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kyanogen.signatureview.SignatureView;

import java.util.List;

import in.exalture.signature.model.User;

public class Signup extends AppCompatActivity {

    Button clear1,clear2,signup;
    TextView result;
    SignatureView s1,s2;
    EditText username;
    Runnable r1;
    Bitmap b1,b2;
    ProgressBar progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        init();
    }
    private void init(){
        username=(EditText)findViewById(R.id.username);
        progress=(ProgressBar)findViewById(R.id.progress);
        result=(TextView)findViewById(R.id.result);
        s1=(SignatureView)findViewById(R.id.signature_view);
        s2=(SignatureView)findViewById(R.id.signature_view2);
        clear1=(Button)findViewById(R.id.clear1);
        clear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s1.clearCanvas();
                result.setText("");
            }
        });
        clear2=(Button)findViewById(R.id.clear2);

        clear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s2.clearCanvas();
                result.setText("");
            }
        });

        signup=(Button)findViewById(R.id.signup);;
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().length()<=3){
                    username.setError("Enter a valied user name");
                    username.requestFocus();
                }else {
                    Toast.makeText(getApplicationContext(),"Processing please wait",Toast.LENGTH_SHORT).show();
                    progress.setVisibility(View.VISIBLE);
                    final int[] sct = {0};
                    final int[] sct2 = {0};
                    r1=     new Runnable() {
                        @Override
                        public void run() {
                            processSignup(sct,sct2);
                        }
                    };
                    r1.run();
                }

            }
        });
    }
    private void processSignup(int[] sct, int[] sct2){
        List<User> ul  =User.find(User.class, "username = ? ", username.getText().toString().trim());
        if(ul!=null?ul.size()>0?true:false:false){
            Toast.makeText(getApplicationContext(),"User name is invalied",Toast.LENGTH_SHORT).show();
        }else {
            b1=s1.getSignatureBitmap();
            for(int i=0;i<s1.getWidth();i++){
                for(int j=0;j<s1.getHeight();j++){
                    int x=i;
                    int y=j;


                    int pixel = b1.getPixel(x,y);
                    if(pixel!=-1){
                        sct[0]++;
                        Log.i("pixel",x+","+y+" = "+pixel);//Toast.makeText(getApplicationContext(),"yes "+pixel,Toast.LENGTH_SHORT).show();
                    }

                }
            }

            b2=s2.getSignatureBitmap();
            for(int i=0;i<s2.getWidth();i++){
                for(int j=0;j<s2.getHeight();j++){
                    int x=i;
                    int y=j;


                    int pixel = b2.getPixel(x,y);
                    if(pixel!=-1){
                        sct2[0]++;
                        Log.i("pixel",x+","+y+" = "+pixel);//Toast.makeText(getApplicationContext(),"yes "+pixel,Toast.LENGTH_SHORT).show();
                    }
                    ;

                }
            }
            int res;
            if(sct[0]>sct2[0]){
                res=sct[0]-sct2[0];
            }else {
                res=sct2[0]-sct[0];
            }
            if(sct[0]==0||sct2[0]==0){
                Toast.makeText(getApplicationContext(),"Signature not matched",Toast.LENGTH_SHORT).show();
            }else {
                Log.i("signup_value",sct[0]+" , "+sct2[0]);
                if(res>50)
                {
                    result.setText("Signature is not matched");
                }
                else {
                    result.setText("Signature is matched");
                    User user=new User();
                    user.setSig(sct[0]);
                    user.setSig2(sct2[0]);
                    user.setUsername(username.getText().toString());

                    user.save();
                    finish();;
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));

                }
            }

        }

        progress.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        r1=null;b1=null;b2=null;s1=null;s2=null;
    }
}
