package in.exalture.signature;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import in.exalture.signature.model.User;

public class HomePage extends AppCompatActivity {
    List<User> ul;
    TextView hm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ul  =User.find(User.class, "logedin = ? ", "1");
        if(ul!=null?ul.size()>0?true:false:false){
            hm=   (TextView)findViewById(R.id.homepagetv);
            hm.setText("Welcome "+ul.get(0).getUsername());
            final FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {


                                           if(ul!=null?ul.size()>0?true:false:false){
                                               ul.get(0).setLogedin(false);
                                               ul.get(0).save();
                                               startActivity(new Intent(getApplicationContext(),LandingPage.class));
                                               finish();
                                           }
                                       }
                                   }
            );

        }else {
            startActivity(new Intent(getApplicationContext(),LandingPage.class));
            finish();
        }


    }

}
